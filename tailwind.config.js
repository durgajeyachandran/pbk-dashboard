module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    spinner: (theme) => ({
      default: {
        color: '#CF4520', // color you want to make the spinner
        size: '70px', // size of the spinner (used for both width and height)
        border: '6px', // border-width of the spinner (shouldn't be bigger than half the spinner's size)
        speed: '650ms', // the speed at which the spinner should rotate
      },
      // md: {
      //   color: theme('colors.red.500', 'red'),
      //   size: '2em',
      //   border: '2px',
      //   speed: '500ms',
      // },
    }),
    screens: {
      'mb-sm': { 'min': '280px' },
      'mb': { 'min': '320px' },
      'tb-sm': { 'min': '768px' },
      'tb': { 'min': '897px' },
      'md': { 'min': '1024px' },
      'xl': { 'min': '1280px' },
      '2xl': { 'min': '1536px' }
    },
    extend: {
      colors: {
        'ed4-beige': '#E4DED4',
        '1e7-beige': '#faf7f0',
        '8ee-beige': '#FFF8EE',
        '7c6-beige': '#DFD7C6',
        '520-orange': '#CF4520',
        'hover-orange': '#F34F2E',
        '161-gray': '#626161',
        'cec-gray': '#ECECEC',
        'bab-gray': '#bababa',
        '279-biscuit': "#BEB279",
        '515-black': "#151515",
        '31e-black': "#28231E",
        '404-black': "#404040"
      },
      boxShadow: {
        '520-orange': '0 0 8px 0 #cf4520aa',
        'bab-gray': '0 0 8px 0 #bababa',
        'sm-white': '0 0 100px 0 #fffdd',
      },
      opacity: {
        '98': '0.98',
      },
      maxHeight: {
        'screen-sm': '360px'
      },
      fontFamily: {
        'fedra-sans-demi': ['"FedraSansStd-Demi"'],
        'fedra-sans-med': ['"FedraSansStd-Medium"']

      },
      margin: {
        '1.6': '.4rem',
        '1.2': '.3rem'
      },
      zIndex: {
        '-1': '-1',

        'max': '1500'
      },
      spacing: {
        '96': '18rem',
        '23': '5.5rem'
      }
    },
  },
  variants: {
    spinner: ['responsive'],
    transitionProperty: ['responsive', 'motion-safe', 'motion-reduce'],
    animation: ['responsive', 'motion-safe', 'motion-reduce'],
    extend: {},
  },
  plugins: [
    require('tailwindcss-spinner')({ className: 'spinner', themeKey: 'spinner' }),
  ],
}


import React, { Suspense } from "react";
import { Router } from "react-router-dom";
import history from "./history";
import Splash from "./components/Splash";
//import IshaSSO from "./components/IshaSSO";
//import useSSO from "./hooks/useSSO";
//import { API_HOST } from "./utils/network"
//import "url-search-params-polyfill";
const AppRoutes = React.lazy(() => import("./routes"));
const AuthenticatedPath = () => {
    const sso = {
        isAuthenticated: true
    };
    if (sso.isAuthenticated === undefined) {
        return <Splash />
    }
    else if (!sso.isAuthenticated) {
        //sso.UserInfo.requestLogin('/', "IF", `${API_HOST}/sso/callback`, `${API_HOST}/sso/getLoginHash`);
        return null;
    }
    return (

        <Suspense fallback={<Splash msg={"Taking you to the right place ..."} />} >
            <AppRoutes />
        </Suspense>

    );
};
const AppRouter = () => {
    return <>
        <Router history={history}>
            {/*<IshaSSO>*/}
                <AuthenticatedPath />
            {/*</IshaSSO>*/}
        </Router>

    </>

}

export default AppRouter;

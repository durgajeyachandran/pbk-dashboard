import React from 'react'

export const SideBarData = [
    {
        title: 'Add Program',
        path: '/',
        icon: '',
        cName: 'nav-text',
    },
    {
        title: 'PRS',
        path: '/program/prs',
        icon: '',
        cName: 'nav-text',
    },
    {
        title: 'Session',
        path: '/program/session',
        icon: '',
        cName: 'nav-text',
    }
]
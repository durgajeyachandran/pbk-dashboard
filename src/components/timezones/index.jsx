import React, {useState} from 'react'

export const TimeZones = () => {
    const [timeZone, setTimeZone] = useState([
        {
            id: 1,
            timeZone: 'IST',
        },
        {
            id: 2,
            timeZone: 'PST',
        },
        {
            id: 3,
            timeZone: 'CEDT',
        }
    ]);

}

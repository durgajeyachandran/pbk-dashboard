import { useState } from "react";
import logo from '../../assets/img/logo192-min.png';
function Splash({
  msg
}) {
  const [imgLoaded, setimgStatus] = useState(false);
  return (<div className={`flex flex-col bg-ed4-beige justify-center fullscreen space-y-24`}>
    <div style={
      {
        filter: 'drop-shadow(2px 2px 2px #00000080)',
        // to hardware accelerate on safari
        WebkitTransform: 'translateZ(0)',
        WebkitPerspective: 1000,
        WebkitBackfaceVisibility: "hidden"
      }}>
      <img className={`${imgLoaded ? 'block' : 'hidden'} h-24 w-24 text-center m-auto`} src={logo} alt="Pancha Bhutha Kriya" onLoad={() => { setimgStatus(true) }} />
    </div>
    <div className={"spinner"} style={{
      height: 'min-content',
      filter: 'drop-shadow(4px 4px 16px #00000070)'

    }}>
    </div>
    <div className={`${!imgLoaded ? 'block' : 'hidden'} hidden text-404-black font-mono italic font-bold text-xl text-center mx-auto pl-4`}>
      {msg || 'Processing ...'}
    </div>
  </div>);
}
export default Splash
import React,{useState} from 'react'
import { Link } from 'react-router-dom'
import { SideBarData } from '../SideBarData';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import "../../assets/css/navbar.css"
import { IconContext } from 'react-icons';

export default function SideNavBar() {
    const [sidebar, setSidebar] = useState(true);
    const showSideBar = () => setSidebar(!sidebar);
    return (
        <>

            <IconContext.Provider value={{ color: '#fff' }}>

            <div className="navbar">
                <Link to="#" className='menu-bars' >
                    <FaIcons.FaBars onClick={showSideBar} />
                </Link>
                </div>
                <h1 className="text-center text-4xl font-semibold mt-10">Dashboad</h1>

            <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                <ul className="nav-menu-items">
                    <li className="navbar-toggle">
                        <Link to="#" className='menu-bars'>
                            <AiIcons.AiOutlineClose onClick={showSideBar} />
                        </Link>
                    </li>
                    {SideBarData.map((item, index) => {
                        return (
                            <li key={index} className={item.cName}>
                                <Link to={item.path}>
                                    <span>{item.title}</span>
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </nav>
            </IconContext.Provider>
        </>
    )
}

import React from 'react';
import { Router, Route, Switch,Redirect } from 'react-router-dom';
import Home from './pages/Home';
import Session from "./pages/Session";
import Prs from "./pages/PRS";
import SideNavBar from "./components/sideNavBar"
import "./styles/sessionStyles.css";
function Routes() {
    return (
        <>
                <SideNavBar />
                    <Switch>

                        <Route exact path="/">
                            <Redirect to="/program" />
                        </Route>
                        <Route exact path="/program">
                            <Home />
                        </Route>
                        <Route path="/program/session">
                            <Session />
                        </Route>
                        <Route path="/program/prs">
                            <Prs />
                        </Route>
                    </Switch>

        </>

)
}

export default Routes;
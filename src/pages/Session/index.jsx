import React, { useState, useEffect } from "react";
import history from "../../history"

function Session() {




    return (
        <>
            <div className="w-full max-w-3xl m-auto  md:px-10 px-5">


                <form
                    className="px-10 border py-10"
                    onSubmit="{handleSubmit(onSubmit)}" style={{"border-color": "black"}}
                >
                    <div className="md:flex">
                        <div className="md:flex-col md:w-1/2 px-2">
                            <label className="text-gray-600 font-medium">Session Name</label>
                            <input
                                className="w-full mt-2 mb-6 py-2 px-4 border rounded-lg text-gray-700 "
                                name="sessionName" type="text"
                            />
                        </div>
                        <div className="md:flex-col md:w-1/2 px-2">

                            <label className="text-gray-600 font-medium">End Time</label>

                            <input
                                className="w-full mt-2 mb-6 py-2 px-4 border rounded-lg text-gray-700"
                                name="sessionEndTime" type="datetime-local"
                            />
                        </div>

                    </div>

                    <div className="md:flex">
                        <div className="md:flex-col md:w-1/2 px-2">

                            <label className="text-gray-600 font-medium">Door Open</label>

                            <input
                                className="w-full mt-2 mb-6 py-2 px-4 border rounded-lg text-gray-700"
                                name="regOpen" type="datetime-local"
                            />
                        </div>
                        <div className="md:flex-col md:w-1/2 px-2">
                            <label className="text-gray-600 font-medium">Door Close</label>

                            <input
                                className="w-full mt-2 mb-6 py-2 px-4 border rounded-lg text-gray-700"
                                name="regClose" type="datetime-local"
                            />
                        </div>

                    </div>
                    <div className="md:flex">
                        <div className="md:flex-col md:w-1/2 px-2">
                            <label className="text-gray-600 font-medium">Session Type</label>
                            <select className="w-full mt-2 mb-6 py-2 px-4 border rounded-lg bg-white">
                                <option>
                                    Live
                                </option>
                                <option>
                                    On Demand
                                </option>
                            </select>
                        </div>
                        <div className="md:flex-col md:w-1/2 px-2">
                            <label className="text-gray-600 font-medium">Program Id</label>
                            <input
                                className="w-full mt-2 mb-6 py-2 px-4 border rounded-lg text-gray-700"
                                name="prsPrgId"

                            />
                        </div>


                    </div>

                </form>
            </div>
        </>
    );
}
export default Session;
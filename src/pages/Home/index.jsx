import id from "date-fns/esm/locale/id/index.js";
import React, { useState, useEffect } from "react";
import history from "../../history";
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import Session from "../Session"

function Home() {

    const [timeZones, setTimeZones] = useState([
        {
            id: 1,
            timeZone: 'IST',
            show: false,
            session: []
        },
        {
            id: 2,
            timeZone: 'PST',
            show: false,
            session: []

        },
        {
            id: 3,
            timeZone: 'CEDT',
            show: false,
            session: []

        }
    ]);

    const [newTz, setnewTz] = useState('');
    const [isSession, setIsSession] = useState(false);

    const setAndSaveTZ = (newTz) => {
        setTimeZones(newTz);
        localStorage.setItem('', JSON.stringify(newTz));
    }
    const addTimeZones = (timeZone) => {
        const id = timeZones.length ? timeZones[timeZones.length - 1].id + 1 : 1;
        const mynewTZ = { id, timeZone, show: false, session: [] };
        const listTZ = [ ...timeZones, mynewTZ ];
        setAndSaveTZ(listTZ);
        console.log(timeZones)


    }
    const toggleCollapse = (id) => {
        const Collapsible = timeZones.map((tz) => tz.id === id ? { ...tz, show: !tz.show } : tz);
        setTimeZones(Collapsible);
    }

    const handleDelete = (id) => {
        const deleteTZ = timeZones.filter((tz) => tz.id !== id );
        setAndSaveTZ(deleteTZ);
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        if (!newTz) return;
        console.log(newTz)
        addTimeZones(newTz);
        setnewTz('');
    }


    return (
        <>
            <div className="w-full max-w-3xl m-auto py-5 mt-10 px-10 ">
                <form className="flex flex-row" onSubmit={(e)=>handleSubmit(e)}>
                    <input
                        className="flex-col md:w-full w-1/2 border-solid border-gray-300 border py-4 px-4 rounded text-gray-700"
                        name="addTimeZone" type="text"
                        autoFocus
                        placeholder="add timezones"
                        value={newTz}
                        onChange={(e) => setnewTz(e.target.value)}

                    />
                    <button  className="flex-col md:w-1/2 w-full  py-4 items-end " type="submit">
                        <span className="bg-520-orange md:px-10 px-2 py-4 text-white rounded-md">Add TimeZone</span>
                    </button>
                </form>



                <div className="rounded-sm">
                    <div>
                        <ul>
                            {
                                timeZones.map((tz) => {
                                    return (
                                        <li key={tz.id} className={`my-5`}>
                                            <div className="flex flex-row w-full border bg-gray-10 px-10 py-5">
                                                <button onClick={() => toggleCollapse(tz.id)} className="flex-col w-1/2  items-start" type="button">
                                                    {tz.timeZone}
                                                </button>

                                                    {
                                                    tz.show ? <>
                                                        <button className="flex-col w-1/2  items-end" type="button">

                                                            <div className="flex">
                                                                <span className="px-5">
                                                                    <FaIcons.FaTrashAlt onClick={() => handleDelete(tz.id)} />
                                                                </span>
                                                                <FaIcons.FaAngleUp onClick={() => toggleCollapse(tz.id)} />
                                                            </div>
                                                        </button>

                                                    </> : <button className="flex-col w-1/2  items-end" type="button" onClick={() => toggleCollapse(tz.id)}>
                                                            <FaIcons.FaAngleDown  />
                                                        </button>

                                                    }

                                            </div>

                                            {
                                                tz.show ? <div className="py-5">
                                                    <div className={`flex w-full ${isSession?'px-10':'px-4'} mb-5 `}>
                                                        <p className="flex-col w-1/2 items-start">
                                                            Sessions
                                                        </p>
                                                        <button onClick={() => setIsSession(true)} className="flex-col w-1/2 items-end">
                                                            Add Session
                                                        </button>

                                                    </div>
                                                    {
                                                        isSession ?
                                                            <Session></Session> : <></>
                                                    }

                                                </div> : <></>
                                            }
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>


                </div>


            </div>

        </>
    );
}
export default Home;
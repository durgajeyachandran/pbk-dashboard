import React, { useState, useEffect } from "react";
import history from "../../history"
function Home() {
    const [prgId, setPrgId] = useState()
    return (
        <>
            <form
            className="w-full max-w-lg m-auto py-10 mt-10 px-10 border"
            >
                <div>
                    <input
                        className="border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                            name="prgId" type="hidden" value={prgId}
                        />
                    <label className="text-gray-600 font-medium py-2 ">Program Type</label>
                    <input
                        className="mb-5 border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                        name="prgType"
                        autoFocus
                    />
                    <label className="text-gray-600 font-medium ">Registration Open</label>

                    <input
                        className="mb-5 border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                        name="regOpen" type="datetime-local"
                        />
                    <label className="text-gray-600 font-medium ">Registration Close</label>

                    <input
                        className="mb-5 border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                        name="regClose" type="datetime-local"
                    />

                </div>
                <button
                    className="mt-4 w-full bg-520-orange hover:bg-hover-orange text-green-100 border shadow py-3 px-6 font-semibold text-md rounded"
                    type="submit"
                >
                    Submit
                </button>
            </form>

        </>
    );
}
export default Home;
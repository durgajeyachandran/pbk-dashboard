import React, { useState, useEffect } from "react";
import history from "../../history"

function Prs() {
    const [prgId, setPrgId] = useState()
    return (
        <>
            <h1 className="text-center text-4xl font-semibold mt-10">PBK Dashboad</h1>
            <form
                className="w-full max-w-lg m-auto py-10 mt-10 px-10 border"
                onSubmit="{handleSubmit(onSubmit)}"
            >
                <div>
                    <h2 className="text-center text-xl font-semibold mt-5">PRS</h2>
                    <label className="text-gray-600 font-medium py-2 ">Program Date</label>
                    <input
                        className="mb-5 border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                        name="regClose" type="month"
                    />
                    <label className="text-gray-600 font-medium py-2 ">Program Id</label>
                    <input
                        className="mb-5 border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                        name="prsPrgId"

                    />

                </div>
                <button
                    className="mt-4 w-full bg-520-orange hover:bg-hover-orange text-green-100 border shadow py-3 px-6 font-semibold text-md rounded"
                    type="submit"
                >
                    Submit
                </button>
            </form>

        </>
    );
}
export default Prs;